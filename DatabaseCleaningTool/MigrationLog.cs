﻿namespace csvImport
{
    using System;
    using System.Text;
    using System.Data;
    public static class MigrationLog
    {
        //static SqlConnection con;
        public static void DisplayError(string ExcMessage)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ExcMessage.ToString());
            Console.ReadLine();
            Environment.Exit(0);
        }
        public static void WriteToCsvFile( Properties Property)
        {
            string fileName = "ImportCSVLog_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            string fullpath = string.Empty;
            StringBuilder fileContent = new StringBuilder();
            foreach (var col in Property.MigrationLogTable.Columns)
            {
                fileContent.Append(col.ToString() + ",");
            }
            fileContent.Replace(",", System.Environment.NewLine, fileContent.Length - 1, 1);

            foreach (DataRow dr in Property.MigrationLogTable.Rows)
            {
                foreach (var column in dr.ItemArray)
                {
                    fileContent.Append("\"" + column.ToString() + "\",");
                }
                fileContent.Replace(",", System.Environment.NewLine, fileContent.Length - 1, 1);
            }
            fullpath = Property.DataLogcsvPath + fileName;
            System.IO.File.WriteAllText(fullpath, fileContent.ToString());
        }
        public static void LogToDatatable(Properties Property)
        {
            DataRow LogRow = Property.MigrationLogTable.NewRow();

            LogRow["StepName"] = Property.stepname;
            LogRow["TargetTable"] = Property.masterTableName;
            LogRow["ExecutionStatus"] = Property.Status;
            LogRow["ExecMessage"] = Property.Message;
            LogRow["CreatedOn"] = DateTime.Now;
            LogRow["CreatedBy"] = "csvTool";
            Property.MigrationLogTable.Rows.Add(LogRow);
        }
    }
}
