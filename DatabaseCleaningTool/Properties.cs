﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace csvImport
{
    public class Properties
    {

        //public string destConstring = ConfigurationManager.ConnectionStrings["SQLAzureConnection"].ToString();
        public string CSVPath = ConfigurationManager.AppSettings["csvFilePath"];
        public string ScriptsFilePath = ConfigurationManager.AppSettings["ScriptsFilePath"];
        public string csvConfigfile = "DecryptEncryptColumns.csv";
        public string csvScriptsConfigfile = "CleanScriptsList.csv";
        public string PartitionKey = ConfigurationManager.AppSettings["ida:PartitionKey"];
        public string RowKey = ConfigurationManager.AppSettings["ida:RowKey"];

        

        //string _srcDBName;
        string _destDBName;
        string _serverName;
        //string _pwd;
        //string _username;
        string _Message;
        string _stepname;
        string _Status;
        string _RunOnlyCSV;
        string _RunFullMigraiton;
        string _StorageAccount;
        string _tablename;
        string _GUIDColumn;
        string _StageTableSql;
        string _errorMethod;
        string _destConn;
        string __approotPath;


        Int16 _IsCSV;
        Int64 _PKUSerID;
        Int16 _IsError;
        Int32 _CursorLeft = 0;
        Int32 _CursorTop = 0;
        Int16 _hasData;

        DataTable _csvTable;
        DataTable _DBLogTable;
        DataTable _tableslistcsv;

        public Properties()
        {
            //srcBuilder = new SqlConnectionStringBuilder(SourceConStr);
            //dstBuilder = new SqlConnectionStringBuilder(destConstring);
            //_srcDBName = srcBuilder.InitialCatalog.ToString();
            //_destDBName = dstBuilder.InitialCatalog.ToString();
            //_serverName = dstBuilder.DataSource.ToString();
            //_pwd = dstBuilder.Password.ToString();
            //_username = dstBuilder.UserID.ToString();
            _RunOnlyCSV = ConfigurationManager.AppSettings["RuncsvOnly"];
            _RunFullMigraiton = ConfigurationManager.AppSettings["RunFullmigration"];
            _StorageAccount = ConfigurationManager.AppSettings["StorageConnection"];
            _PKUSerID = Convert.ToInt64(ConfigurationManager.AppSettings["PKUserID"]);
        }

        public string destConstring
        {
            get { return _destConn; }
            set { _destConn = value; }
        }
        public Int32 CursorLeft
        {
            get { return _CursorLeft; }
            set { _CursorLeft = value; }
        }
        public Int32 CursorTop
        {
            get { return _CursorTop; }
            set { _CursorTop = value; }
        }
        public Int16 hasData
        {
            get { return _hasData; }
            set { _hasData = value; }

        }
        public Int16 IsError
        {
            get { return _IsError; }
            set { _IsError = value; }
        }
        public Int16 IsCSV
        {
            get { return _IsCSV; }
            set { _IsCSV = value; }
        }
        public Int64 PKUSerID
        {
            get { return Convert.ToInt64(_PKUSerID); }
        }
        public string ErrorMethod
        {
            get { return _errorMethod; }
            set { _errorMethod = value; }
        }
        public DataTable  csvTable
        {
            get { return _csvTable; }
            set { _csvTable = value; }
        }
        public DataTable MigrationLogTable
        {
            get { return _DBLogTable; }
            set { _DBLogTable = value; }
        }
        public DataTable MasterTables
        {
            get { return _tableslistcsv; }
            set { _tableslistcsv = value; }
        }
        public string StageTableSql
        {
            get { return _StageTableSql; }
            set { _StageTableSql = value; }
        }
        public string masterTableName
        {
            get { return _tablename; }
            set { _tablename = value; }
        }
        public string GUIDColumn
        {
            get { return _GUIDColumn; }
            set { _GUIDColumn = value; }

        }
        public string TablesListCSV
        {
            get { return CSVPath + csvConfigfile; }
        }
        public string CleanScriptsListCSV
        {
            get { return ScriptsFilePath + csvScriptsConfigfile; }
        }
        public string ApprootPath
        {
            get { return __approotPath; }
            set { __approotPath = value; }
        }

        public string DataLogcsvPath
        {
            get { return ApprootPath + "ExecutionLog\\"; }
            //get { return ConfigurationManager.AppSettings["DatalogcsvPath"]; }
        }
        public string AzureStorageAccount
        {
            get { return _StorageAccount; }
        }
        public string MigraitonExecOption
        {
            get
            {
                string Option = string.Empty;
                if (ConfigurationManager.AppSettings["RunFullmigration"] == "1")
                {
                    Option = "2";
                }
                else
                if (ConfigurationManager.AppSettings["RuncsvOnly"] == "1")
                {
                    Option = "1";
                }
                return Option;
            }
        }
        public string destDBName
        {
            get { return _destDBName; }
            set { _destDBName = value; }
        }
        public string Servername
        {
            get { return _serverName; }
            set { _serverName=value; }

        }
        //public string Password
        //{
        //    get { return _pwd; }
        //}
        //public string UserName
        //{
        //    get { return _username; }
        //}
        public string stepname
        {
            get { return _stepname; }
            set { _stepname = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public string SQLPathbuild(string migOption)
        {
            string retSQL = string.Empty;
            switch (migOption)
            {
                case "0":
                    retSQL = ApprootPath + "configs\\sqlscripts\\";
                    break;
                    //case "2":
                    //    retSQL = ApprootPath + "configs\\b2csqlscripts\\";
                    //    break;
                    //case "1":
                    //    retSQL = ApprootPath + "configs\\sqlscripts\\";
                    //break;
            };
            return retSQL;
        }
        public string SQLPath
        {
            get { return SQLPathbuild("0"); }//As this solution is for only .csv file import so passing a constant "0"
        }
    }
}
