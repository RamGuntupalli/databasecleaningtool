﻿namespace csvImport
{
    using Microsoft.Azure.KeyVault;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using Microsoft.WindowsAzure.Storage.Table;
    using System.Linq;
    using System.Security.Cryptography.X509Certificates;
    using System.Text.RegularExpressions;
    using Microsoft.IdentityModel.Clients.ActiveDirectory;
    using System.Security.Cryptography.X509Certificates;
    using System.Threading.Tasks;
    using System.Text;
    using System.Security.Claims;
    using FINAPI.Utilities;

    public class KeyVaultAccess: Menu 
        {
            const string CLIENT_ID = "";
            const string CLIENT_SECRET = "";
            static string returnvalue = string.Empty;
            static string url = string.Empty;
            static List<Subscriptions> lsitSubscriptions;
            public string GetSQLConnection(Properties Property)
            {
                try
                {
                    Property.stepname = "Fetch SQL Connection";
                    Property.Status = "Start";
                    Property.Message = "Import SQL Connection string from KeyVault";
                    MigrationLog.LogToDatatable(Property);

                    GetSubscriptionsTable(Property);
                var keyVaultClient = new KeyVaultClient(CertificateAuthenticateVault);

                var result = keyVaultClient.GetSecretAsync(url + "clean-db-connection-string").Result;
                string conn = result.Value.ToString();
                return conn;
                }
                catch (Exception ex)
                {
                    throw new ArgumentException(ex.Message.ToString());
                }
                finally { }
            }
            private void GetSubscriptionsTable(Properties Property)
            {
                try
                {
                    Property.stepname = "Read Subscriptions table (Azure table)";
                    Property.Status = "Start";
                    Property.Message = "Fetch the required values to access Keyvault url (stored in Subscription azure table)";
                    MigrationLog.LogToDatatable(Property);

                    string _storageKey = ConfigurationManager.AppSettings["ida:StorageKey"];
                    string _storageAccountName = ConfigurationManager.AppSettings["ida:StorageAccount"];
                    string clientCert = string.Empty;
                    if (string.IsNullOrEmpty(_storageKey) || string.IsNullOrEmpty(_storageAccountName))
                    {
                        throw new System.ArgumentException("The account and keys are not initialized");
                    }
                    if (string.IsNullOrEmpty(Property.PartitionKey) || string.IsNullOrEmpty(Property.RowKey))
                    {
                        throw new System.ArgumentException("PartionKey/RowKey not defeind in app.config");
                    }

                    AzureTableStorageHelper helper = new AzureTableStorageHelper(_storageAccountName, _storageKey);
                    var table = helper.Client.GetTableReference("Subscriptions");
                    string partitionFilter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, Property.PartitionKey);
                    string rowFilter = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, Property.RowKey);
                    TableQuery<Subscriptions> Query = new TableQuery<Subscriptions>().Where(
                                                            TableQuery.CombineFilters(
                                                                                        TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, Property.PartitionKey),
                                                                                        TableOperators.And,
                                                                                        TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, Property.RowKey)
                                                                                        )
                                                            );
                    lsitSubscriptions = new List<Subscriptions>();
                    Subscriptions sub = new Subscriptions();
                    foreach (var item in table.ExecuteQuery(Query))
                    {
                    sub.KeyVaultUrl = item.KeyVaultUrl;
                    sub.PartitionKey = item.PartitionKey;
                    sub.RowKey = item.RowKey;
                    sub.B2CTenantId = item.B2CTenantId;
                    sub.AzureTenantId = item.AzureTenantId;
                    sub.CertificateThumbprint = item.CertificateThumbprint;
                    lsitSubscriptions.Add(sub);
                    url = item.KeyVaultUrl;
                }
                    Property.stepname = "Read Subscriptions table (Azure table)";
                    Property.Status = "End";
                    Property.Message = "Fetch the required values to access Keyvault url (stored in Subscription azure table)";
                    MigrationLog.LogToDatatable(Property);
            }
                catch (Exception ex)
                {
                    Property.stepname = "ValidateTable";
                    Property.Status = "Error";
                    Property.Message = "Validate target tables based on tableslist.csv (validate hasData)::" + ex.Message.ToString();
                    MigrationLog.LogToDatatable(Property);
                    if (Property.ErrorMethod == null)
                    {
                        Property.ErrorMethod = Property.stepname;
                        DisplayError(Property.Message);
                    }
            }
              
            }
            public static async Task<string> CertificateAuthenticateVault(string authority, string resource, string scope)
            {
                //var Subscriptions = GetSubscriptionsTable();

                var SQLCertificateThumbPrint = lsitSubscriptions[0].CertificateThumbprint.ToString();
           
                var certificate = GetCertificate(SQLCertificateThumbPrint);
                var clientAssertionCertificate = new ClientAssertionCertificate(ConfigurationManager.AppSettings["ida:ClientId"], certificate);
                var authenticationContext = new AuthenticationContext(authority);
                var result = await authenticationContext.AcquireTokenAsync(resource, clientAssertionCertificate);
                returnvalue = result.AccessToken;
                return result.AccessToken;
            }
            private static X509Certificate2 GetCertificate(String thumbprint)
            {
                returnvalue = "0";

                thumbprint = Regex.Replace(thumbprint, @"[^\da-zA-z]", string.Empty).ToUpper();
                X509Certificate2 certificater;
                X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                try
                {
                    store.Open(OpenFlags.ReadOnly);
                    X509Certificate2Collection certificateCollection = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);
                    if (certificateCollection == null || certificateCollection.Count == 0)
                    {
                        throw new Exception("Certificate not installed in the store");
                    }
                    certificater = certificateCollection[0];
                }
                finally
                {
                    store.Close();
                }
                return certificater;
            }

        public string ToDecrypt(string fields)
        {

            var plainTaxIdentifier = "";
            if (fields != null && fields != "" && fields != "NULL" && fields != " ")
            {

                using (var keyVaultClient1 = new KeyVaultClient(CertificateAuthenticateVault))
                {
                    var result = keyVaultClient1.GetSecretAsync(url + "clean-db-connection-string").Result;
                    IEnumerable<Claim> claims = ClaimsPrincipal.Current.Claims;
                    var iCompleteEncodedKey = "";
                    
                        var result2 = keyVaultClient1.GetSecretAsync(url + "db-encryption-key").Result;
                        if (result2 != null)
                        {
                            iCompleteEncodedKey = result2.Value;
                            
                        }
                   
                    if (iCompleteEncodedKey.Length > 0 && fields != null && fields.Length > 0)
                    {
                        byte[] IV = Convert.FromBase64String(ASCIIEncoding.UTF8.GetString(Convert.FromBase64String(iCompleteEncodedKey.ToString())).Split(',')[0]);
                        byte[] key = Convert.FromBase64String(ASCIIEncoding.UTF8.GetString(Convert.FromBase64String(iCompleteEncodedKey.ToString())).Split(',')[1]);
                        var encryptedBytes = Convert.FromBase64String(ASCIIEncoding.UTF8.GetString(Convert.FromBase64String(fields)));
                        plainTaxIdentifier = DatabaseColumnsEncryption.DecryptStringFromBytes_Aes(encryptedBytes, key, IV);
                    }
                }
            }
            return plainTaxIdentifier;
        }
        public string ToEncrypt(string fields)
        {
            var plainTaxIdentifier = "";
            if (fields != null && fields != "" && fields != "NULL" && fields != " ")
            {
                using (var keyVaultClient = new KeyVaultClient(CertificateAuthenticateVault))
                {
                    var cleanDBConnectionString = keyVaultClient.GetSecretAsync(url + "clean-db-connection-string").Result;
                    var iCompleteEncodedKey = "";
                    var cleanDBEncryptionkey = keyVaultClient.GetSecretAsync(url + "clean-db-encryption-key").Result;
                  
                        if (cleanDBEncryptionkey != null)
                        {
                            iCompleteEncodedKey = cleanDBEncryptionkey.Value;
                        }
                
                    
                    if (iCompleteEncodedKey.Length > 0 && fields != null && fields.Length > 0)
                    {
                        byte[] IV = Convert.FromBase64String(ASCIIEncoding.UTF8.GetString(Convert.FromBase64String(iCompleteEncodedKey.ToString())).Split(',')[0]);
                        byte[] key = Convert.FromBase64String(ASCIIEncoding.UTF8.GetString(Convert.FromBase64String(iCompleteEncodedKey.ToString())).Split(',')[1]);
                        plainTaxIdentifier = DatabaseColumnsEncryption.EncryptStringToBytes_Aes(fields, key, IV);
                    }
                }
            }
            return plainTaxIdentifier;
        }

    }

        public class Subscriptions : TableEntity
        {
            public string KeyVaultUrl { get; set; }
            public string AzureTenantId { get; set; }
            public string B2CTenantId { get; set; }
            public string CertificateThumbprint { get; set; }
            public string WebUrl { get; set; }
        }

   
}
