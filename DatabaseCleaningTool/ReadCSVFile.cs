﻿namespace csvImport
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using DatabaseCleaningTool;
    using Microsoft.SqlServer.Management.Common;
    using Microsoft.SqlServer.Management.Smo;
    public class ReadCSVFile : Menu
    {
        string ColumnName = string.Empty;
        string RowName = string.Empty;
        string isActive = string.Empty;
        string tableName = string.Empty;
        SqlConnection con;
        SqlTransaction sr = null;
        SqlCommand command;
        DataTable dtCopy = null;
        SqlDataReader reader = null;
        String connectionstring = String.Empty;

        public void ProcessCSVFiles(Properties Property)
        {
            try
            {
                #region Load data into a MasterTable from tableslist.csv, it has list of tables to import

                if (Property.TablesListCSV != null)
                {
                    Property.MasterTables = ReadCsvFile(Property, Property.TablesListCSV, true);
                }
                //if (Property.ScriptsFilePath != null)
                //{
                //    Property.MasterTables = ReadCsvFile(Property, Property.CleanScriptsListCSV, true);
                //}
                dtCopy = Property.MasterTables;
                connectionstring = Property.destConstring;

                KeyVaultAccess decrypt = new KeyVaultAccess();

                Console.WriteLine("{0}", "\n\n\t Executing: Importing .csv files to the Destination Database,and Decrypting and updating the existing data..\n");
                foreach (DataRow dr in dtCopy.Rows)
                {
                    using (con = new SqlConnection(connectionstring))
                    {
                        try
                        {
                            con.Open();
                            sr = con.BeginTransaction();
                            isActive = dr["IsActive"].ToString();
                            ColumnName = dr["ColumnName"].ToString();
                            RowName = dr["PrimaryKey"].ToString();
                            tableName = dr["TableName"].ToString();
                            string sqlQuery = @"select " + RowName + "," + ColumnName + " from " + tableName;
                            command = new SqlCommand(sqlQuery, con, sr);

                            reader = command.ExecuteReader();

                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    //Console.WriteLine("{0} {1}", reader[0], reader[1]);
                                    string dbValue = Convert.ToString(reader[1]);
                                    string identityValue = Convert.ToString(reader[0]);
                                    if (!String.IsNullOrEmpty(dbValue) && isActive == "1")
                                    {
                                        string decryptValue = decrypt.ToDecrypt(dbValue);
                                        string updateSqlQuery = @"update " + tableName + " SET " + ColumnName + " = " + @"'" + decryptValue + @"'" + " where " + RowName + " = " + identityValue;
                                        
                                        using (command = new SqlCommand(updateSqlQuery, con, sr))
                                        {
                                            //command.CommandTimeout = 10000;
                                            command.ExecuteScalar();
                                        }
                                    }
                                }
                                Console.WriteLine("{0}", "{1}", "Successfully decrypted the" + ColumnName + "of" + tableName);
                                //console.WriteLine(con.State.ToString());
                            }

                            else
                            {
                                Console.WriteLine("No rows found.");
                            }

                            //Console.Write("Done!\n");

                            sr.Commit();
                            Console.WriteLine("{0}","{1}", "Successfully updated"+ColumnName+"in the table"+tableName);
                            con.Close();
                            
                        }
                        catch (Exception ex)
                        {
                            sr.Rollback();
                            Property.stepname = "ProcessCSVFiles";
                            Property.Message = ex.InnerException.Message;
                            Property.Status = "Failed";
                            MigrationLog.LogToDatatable(Property);
                        }
                    }

                    Console.WriteLine("{0}", "\n\n\t Decrypted the data to target database successfully. \n");
                }

                Property.stepname = "ProcessCSVFiles";
                Property.Message = "Successfully decrypted the existing data from database and updated as well...";
                Property.Status = "Success";
                MigrationLog.LogToDatatable(Property);
                #endregion
            }
            catch (Exception ex)
            {
                Property.stepname = "ProcessCSVFiles";
                Property.Status = "Failed";
                Property.Message = ex.Message.ToString();
                MigrationLog.LogToDatatable(Property);
                if (Property.ErrorMethod == null)
                {
                    Property.ErrorMethod = Property.stepname;
                    DisplayError(Property.Message);
                }
                throw new ArgumentException(Property.Message);
            }
            finally { }


        }
        public void ExecuteSQlScript(Properties Property)
        {
            Console.Write("\n\n\t Deploy SQL Scripts to the Destination Database: {0}...", Property.destDBName.ToString());
            string SQLPath = Property.SQLPath;
            string destconstr = Property.destConstring;
            try
            {
                Property.MasterTables = ReadCsvFile(Property, Property.CleanScriptsListCSV, true);
                DataTable dtCopy = Property.MasterTables;
                List<SqlScript> MasterTables = new List<SqlScript>();
                MasterTables = (from DataRow dr in dtCopy.Rows
                                select new SqlScript()
                                {
                                    ScriptFileName = dr["ScriptFileName"].ToString(),
                                    IsActive = Convert.ToString(dr["IsActive"]),
                                }).ToList();
                Console.WriteLine("Suucessfully imported .CSV files in to temparory table");
                Console.WriteLine("Processing ececuting the sql script...");
                foreach (var Table in MasterTables)
                {
                    //DirectoryInfo di = new DirectoryInfo(objProp.ScriptsFilePath);
                    string filepath = Property.ScriptsFilePath + Table.ScriptFileName;
                    ExecSQLBatch(Property, filepath);
                    Console.WriteLine("{0}","Successfully executed the"+ Table.ScriptFileName);
                }
                Property.stepname = "ExecuteSQlScript";
                Property.Message = "Succesfully Executed the scripts";
                Property.Status = "Success";
                MigrationLog.LogToDatatable(Property);

            }
            catch (Exception ex)
            {
                Property.stepname = "ExecuteSQlScript";
                Property.Message = ex.InnerException.Message;
                Property.Status = "Failed";
                MigrationLog.LogToDatatable(Property);

                DisplayError(ex.InnerException.ToString());
                //Environment.Exit(0);
            }
            finally
            {
                con.Dispose();
                Console.Write("Completed!\n");
            }
        }
        public void ExecSQLBatch(Properties objProp, string fileName)
        {
            Console.Write("\n\n\t Deploy SQL Scripts to the Destination Database: {0}...", objProp.destDBName.ToString());
            string SQLPath = objProp.SQLPath;
            string destconstr = objProp.destConstring;
            try
            {
                con = new SqlConnection(destconstr);
                FileInfo file = new FileInfo(fileName);
                string script = file.OpenText().ReadToEnd();
                Server server = new Server(new ServerConnection(con));
                server.ConnectionContext.ExecuteNonQuery(script);
                con.Close();
            }
            catch (Exception ex)
            {
                
                DisplayError(ex.InnerException.ToString());
                //Environment.Exit(0);
            }
            finally
            {
                con.Dispose();
                Console.Write("Completed!\n");
            }
        }
        public void EncryptCleanData()
        {
            KeyVaultAccess decrypt = new KeyVaultAccess();
            Properties Property = new Properties();
            Console.WriteLine("Encrypting the data to targeted database.");
            foreach (DataRow dr in dtCopy.Rows)
            {
                using (con = new SqlConnection(connectionstring))
                {
                    try
                    {
                        con.Open();
                        sr = con.BeginTransaction();
                        isActive = dr["IsActive"].ToString();
                        ColumnName = dr["ColumnName"].ToString();
                        RowName = dr["PrimaryKey"].ToString();
                        tableName = dr["TableName"].ToString();
                        string sqlQuery = @"select " + RowName + "," + ColumnName + " from " + tableName;
                        command = new SqlCommand(sqlQuery, con, sr);

                        reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                string dbValue = Convert.ToString(reader[1]);
                                string identityValue = Convert.ToString(reader[0]);
                                if (!String.IsNullOrEmpty(dbValue) && isActive == "1")
                                {
                                    string decryptValue = decrypt.ToEncrypt(dbValue);
                                    string updateSqlQuery = @"update " + tableName + " SET " + ColumnName + " = " + @"'" + decryptValue + @"'" + " where " + RowName + " = " + identityValue;
                                    Console.WriteLine(identityValue);
                                    using (command = new SqlCommand(updateSqlQuery, con, sr))
                                    {
                                        //command.CommandTimeout = 10000;
                                        command.ExecuteScalar();
                                    }
                                }
                            }
                            Console.WriteLine("{0}", "{1}", "Successfully encrypted the" + ColumnName + "of" + tableName);
                        }
                        else
                        {
                            Console.WriteLine("No rows found.");
                        }

                        //Console.Write("Done!\n");
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                        sr.Rollback();
                        DisplayError(ex.InnerException.ToString());
                        Property.stepname = "EncryptCleanData";
                        Property.Message = ex.InnerException.Message;
                        Property.Status = "Failed";
                        MigrationLog.LogToDatatable(Property);
                        
                    }
                }
                
                Property.stepname = "EncryptCleanData";
                Property.Message = "Encrypted the existing data from database";
                Property.Status = "Success";
                MigrationLog.LogToDatatable(Property);
                sr.Commit();
                Console.WriteLine("{0}", "\n\n\t Encrypted the data to target database successfully. \n");
            }
        }

        public DataTable ReadCsvFile(Properties Property, string csvFile, bool isMasterTable)
        {
            DataTable dtCsv = new DataTable();
            string Fulltext;
            Int64 ColumnsCount = 0;
            string FileSaveWithPath = csvFile;
            try
            {
                using (StreamReader sr = new StreamReader(FileSaveWithPath))
                {
                    List<Int32> lstJSONIndexes = new List<Int32>();
                    while (!sr.EndOfStream)
                    {
                        Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                        string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = SplitCSV(rows[i]); //split each row with comma to get individual values  
                            {
                                if (i == 0)
                                {
                                    for (int j = 0; j < rowValues.Count(); j++)
                                    {
                                        dtCsv.Columns.Add(rowValues[j].Trim()); //add headers  
                                        if (rowValues[j].ToUpper().Contains("JSON")) //"Json1, json2"
                                        {
                                            lstJSONIndexes.Add(j);
                                        }
                                    }
                                    ColumnsCount = dtCsv.Columns.Count;
                                    //if (isMasterTable == true)
                                    //{
                                    //    dtCsv.Columns["InsOrder"].DataType = typeof(Int64);
                                    //}
                                }
                                else
                                {
                                    DataRow dr = dtCsv.NewRow();
                                    for (int k = 0; k < ColumnsCount; k++)
                                    {
                                        if (rowValues[k] == "\r")
                                        {
                                            dr[k] = DBNull.Value;
                                        }
                                        else
                                        {
                                            //dr[k] = rowValues[k].Trim().ToString();

                                            string rValue = rowValues[k].Trim().ToString();
                                            if (rValue.Length > 0)
                                            {
                                                if (lstJSONIndexes.ToArray().Contains(k))
                                                {
                                                    rValue = rValue.Replace("\"\"", "\"").ToString().Trim();
                                                    dr[k] = rValue.Substring(1, rValue.Length - 2).ToString();
                                                }
                                                else
                                                {
                                                    if (rValue.Substring(rValue.Length - 1, 1) == "\"")
                                                    {
                                                        rValue = rValue.Remove(rValue.Length - 1, 1);
                                                    }
                                                    if (rValue.Substring(0, 1) == "\"")
                                                    {
                                                        rValue = rValue.Remove(0, 1);
                                                    }

                                                    dr[k] = rValue.Replace("\"\"", "\"").ToString().Trim();
                                                }
                                            }
                                            else
                                            {
                                                dr[k] = DBNull.Value;
                                            }
                                        }
                                    }
                                    dtCsv.Rows.Add(dr); //add other rows  
                                }
                            }
                        }
                    }
                }

                #region Add GUID & Auditcolumns to the data table
                string GUIDColumn = Property.GUIDColumn;

                if (!string.IsNullOrEmpty(GUIDColumn))
                {
                    DataColumn newCol = new DataColumn(GUIDColumn, typeof(string));
                    dtCsv.Columns.Add(newCol);
                    foreach (DataRow row in dtCsv.Rows)
                    {
                        row[GUIDColumn] = Guid.NewGuid().ToString();
                    }
                }
                if (Property.masterTableName != null)
                {
                    #region Add AuditColumn to the Stage table
                    if (CheckAuditColumns(Property, "CreatedBy") == true)
                    {
                        DataColumn newCol = new DataColumn("CreatedBy", typeof(Int64));
                        dtCsv.Columns.Add(newCol);
                        foreach (DataRow row in dtCsv.Rows)
                        {
                            row["CreatedBy"] = Property.PKUSerID;
                        }
                    }
                    if (CheckAuditColumns(Property, "UpdatedBy") == true)
                    {
                        DataColumn newCol = new DataColumn("UpdatedBy", typeof(Int64));
                        dtCsv.Columns.Add(newCol);
                        foreach (DataRow row in dtCsv.Rows)
                        {
                            row["UpdatedBy"] = Property.PKUSerID;
                        }
                    }
                    DateTime CrrentDate = DateTime.Now;

                    if (CheckAuditColumns(Property, "CreatedOn") == true)
                    {
                        DataColumn newCol = new DataColumn("CreatedOn", typeof(DateTime));
                        dtCsv.Columns.Add(newCol);
                        foreach (DataRow row in dtCsv.Rows)
                        {
                            row["CreatedOn"] = CrrentDate;
                        }
                    }
                    if (CheckAuditColumns(Property, "UpdatedOn") == true)
                    {
                        DataColumn newCol = new DataColumn("UpdatedOn", typeof(DateTime));
                        dtCsv.Columns.Add(newCol);
                        foreach (DataRow row in dtCsv.Rows)
                        {
                            row["UpdatedOn"] = CrrentDate;
                        }
                    }
                    #endregion
                }
                #endregion

                Property.stepname = "Read csv file " + Property.masterTableName;
                Property.Status = "Completed";
                Property.Message = "Read csv file and prepare data table based on csv file.";
                MigrationLog.LogToDatatable(Property);
                return dtCsv;
            }
            catch (Exception Ex)
            {
                Property.stepname = "ReadCsvFile";
                Property.Status = "Error";
                Property.Message = Ex.Message.ToString();
                MigrationLog.LogToDatatable(Property);
                MigrationLog.LogToDatatable(Property);
                if (Property.ErrorMethod == null)
                {
                    Property.ErrorMethod = Property.stepname;
                    DisplayError(Property.Message);
                }
                throw new ArgumentException(Property.Message);
            }
            finally
            {

            }
        }

        public static string[] SplitCSV(string input)
        {
            Regex csvSplit = new Regex("(?:^|,)(\"(?:[^\"]+|\"\")*\"|[^,]*)", RegexOptions.Compiled);
            List<string> list = new List<string>();
            string curr = null;
            foreach (Match match in csvSplit.Matches(input))
            {
                curr = match.Value;
                if (0 == curr.Length)
                {
                    list.Add("");
                }

                list.Add(curr.TrimStart(','));
            }

            return list.ToArray();
        }
        public bool CheckAuditColumns(Properties Property, string AuditColumn)
        {
            bool AuditExists = false;
            try
            {
                string sql = "SELECT 1 as Exist FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name ='" + Property.masterTableName + "' and  Column_name = '" + AuditColumn + "' ";
                con = new SqlConnection(Property.destConstring);
                //KeyVaultAccess KeyVaultOp = new KeyVaultAccess();
                //con.AccessToken = KeyVaultOp.GetAccessTokenByCertificate();

                con.Open();
                SqlCommand com = new SqlCommand(sql, con);
                com.CommandType = CommandType.Text;
                if (Convert.ToInt16(com.ExecuteScalar()) == 1)
                {
                    AuditExists = true;
                }
                con.Close();
            }
            catch (Exception ex)
            {
                Property.stepname = "CheckAuditColumns";
                Property.Status = "Error";
                Property.Message = ex.Message.ToString();
                MigrationLog.LogToDatatable(Property);
                DisplayError(Property.Message);
                if (Property.ErrorMethod == null)
                {
                    Property.ErrorMethod = Property.stepname;
                    DisplayError(Property.Message);
                }
                throw new ArgumentException(Property.Message);
            }
            finally
            {
                con.Dispose();
            }

            return AuditExists;
        }
    }
}