﻿namespace DatabaseCleaningTool
{
    using csvImport;
    using System;
    using System.Configuration;
    using System.Data;
    using System.IO;

    class Program
    {
        static void Main(string[] args)
        {
            ConfigurationManager.RefreshSection("appSettings");
            Console.Title = "DB Cleaning Tool --- DecryptEncryptImport .csv files to SQL Database";
            //AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);
            Properties Property = new Properties();
            Property.MigrationLogTable = MigrationLogTable();

            Menu objMenu = new Menu();
            Property.ApprootPath = AppDomain.CurrentDomain.BaseDirectory;


            if (!Directory.Exists(Property.DataLogcsvPath))
            {
                Directory.CreateDirectory(Property.DataLogcsvPath);
            }

            objMenu.DisplayConfiguration(Property, "Y");
            Property.stepname = ".csv Import";
            Property.Status = "Start";
            Property.Message = "Import .csv files data to Destination database process;";
            MigrationLog.LogToDatatable(Property);
            ReadCSVFile rcsv = new ReadCSVFile();
            rcsv.ProcessCSVFiles(Property);
            rcsv.ExecuteSQlScript(Property);
            rcsv.EncryptCleanData();
            Property.stepname = "Data Base Cleaning Tool";
            Property.Status = "Completed";
            Property.Message = "***Data base cleaning successfully completed***";
            Property.masterTableName = null;
            MigrationLog.LogToDatatable(Property);
            objMenu.DisplayCompleted("*****Import .csv Files Completed Successfully!****");

            #region Create log file .csv
            MigrationLog.WriteToCsvFile(Property);
            #endregion
        }
        static DataTable MigrationLogTable()
        {
            DataTable logTable = new DataTable();
            logTable.Clear();
            DataColumn column = new DataColumn("PKExecutionLogID");
            column.DataType = System.Type.GetType("System.Int64");
            column.AutoIncrement = true;
            column.AutoIncrementSeed = 1;
            column.AutoIncrementStep = 1;
            logTable.Columns.Add(column);
            logTable.Columns.Add("StepName", typeof(string));
            logTable.Columns.Add("TargetTable", typeof(string));
            logTable.Columns.Add("ExecutionStatus", typeof(string));
            logTable.Columns.Add("ExecMessage", typeof(string));
            logTable.Columns.Add("CreatedOn", typeof(DateTime));
            logTable.Columns.Add("CreatedBy", typeof(string));

            return logTable;
        }
    }
}