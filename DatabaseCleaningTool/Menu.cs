﻿namespace csvImport
{
    using System;
    using System.Data.SqlClient;
    public class Menu:Properties 
    {
        //clsProperties objProp;
        public void PrintStar(Int64 Number, string strChar)
        {
            Console.Write("\t");
            while (Number != 0)
            {
                Console.Write(strChar);
                Number = Number - 1;
            }
        }
        
        public void DisplayError(string ErrorMsg)
        {
            Console.Write("\n");
            Console.ForegroundColor = ConsoleColor.Red;
            ErrorMsg.Replace("\r", string.Empty );
            ErrorMsg.Replace("\n", string.Empty);

            Console.WriteLine("\n\t\t"+ErrorMsg);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void DisplayConfiguration(Properties Property, string IsCsvImport)
        {
            Console.ForegroundColor = ConsoleColor.Gray;

            try
            {
                KeyVaultAccess readCon = new KeyVaultAccess();
                Property.destConstring = readCon.GetSQLConnection(Property);

                if (Property.destConstring.Length == 0)
                {
                    throw new Exception("Database Server Connection string not availble (Check if the Key Vault url is accessable).");
                }

                SqlConnectionStringBuilder dstBuilder = new SqlConnectionStringBuilder(Property.destConstring); ;
                Property.Servername = dstBuilder.DataSource.ToString();
                Property.destDBName = dstBuilder.InitialCatalog.ToString();

                if (string.IsNullOrEmpty(Property.Servername) || string.IsNullOrEmpty(Property.destDBName) )
                {
                    throw new System.ArgumentException("Connection string not configures");
                }

                string Option = Property.MigraitonExecOption;
                string ConsoleMsg = string.Empty;
                Console.WriteLine("{0}", "\n\n\t Executing: Import .csv files to the Destination Database.\n");
                PrintStar(70, "*");
                Console.WriteLine("\n\t APP CONFIGURATIONS:");
                PrintStar(70, "*");
                Console.WriteLine("\n\t Servername           : {0}\n\t Destination Database : {1}\n\t" +
                                    " \n\t csv files path (Client config data): {2}\n\t " 
                                        , Property.Servername, Property.destDBName, Property.CSVPath);
               
                PrintStar(70, "*");
                Console.Write("\n\n\tPlease verify above parameters.");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(" Press Enter to continue...");
                Console.ForegroundColor = ConsoleColor.Gray;
                
                ConsoleKeyInfo cnslKey = Console.ReadKey();

                if (cnslKey.Key == ConsoleKey.Escape)
                {
                    Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {
                Property.stepname = "DisplayConfiguration";
                Property.Status = "Error";
                Property.Message = ex.Message.ToString();
                MigrationLog.LogToDatatable(Property);
                DisplayError(ex.Message.ToString());
                throw new ArgumentException(Property.Message);
            }
        }
        public void DisplayCompleted(string Message)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\n\n\t {0}", Message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }

}